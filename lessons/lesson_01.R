
### LESSON 01
## PLOTTING AND STATS
## EXPONENTIAL GROWTH
## LINEAR & NON-LINEAR REGRESSION

### WARMUP: plotting and generating random numbers

## generate a sine wave
x <- (1:100)/100 *2*pi # EXC: what does the ":" do? try the same using seq()
y <- sin(x)

## plot and manipulate axes
plot(x, y, type="l", lwd=2, axes=FALSE, ylab="my sine", xlab="my time")
axis(1)
axis(2)
axis(3, at=seq(0,2*pi,pi/2),labels=expression(0,  pi/2, pi, 3*pi/2, 2*pi),
     col.ticks=2, col=2, col.axis=2)
mtext("my second axis", 4, font=2, col="red",cex=2)


## add normally distributed noise

yn <- y+rnorm(length(y), sd=0.5) # strong noise
lines(x,yn,col="#FF0000")

yn <- y+rnorm(length(y), sd=0.1) # weaker noise
lines(x,yn,col="darkgreen")

## add 'jitter' 
yj <- jitter(y,amount=0.1)
lines(x,yj,col="blue")

## Cool but what did we just do?
## intermezzo: creating normal distributions in R
breaks <- seq(from = -10, to = 10, by = 0.5)
hist(rnorm(10000), breaks = breaks, col = "grey")
hist(rnorm(10000, mean=3), breaks = breaks, add = TRUE, border = 2)
hist(rnorm(10000, sd=2), breaks = breaks, add = TRUE, col = "#0000FF55") # Q: what is "#0000FF55" ?
legend("topright", legend=c("mean=0; sd=1","mean=3; sd=1","mean=0; sd=2"),
       fill=c("gray", NA, "#0000FF55"), border=c(1,2,1))


### MATRIX vs. DATA FRAME
## a matrix must have a certain type eg. all numbers or all strings,
## the data.frame is the equivalent of a "sheet" in m$ excel and can contain
## arbitraty data types

## generate a matrix
xy <- cbind(x,y)

## generate a data.frame
df <- data.frame(x=x,y=y)

## can be plotted directly
plot(df, pch = 20)


### EXPONENTIAL GROWTH and LINEAR MODELS

## X(t) = X(0) * exp(mu*time)
## ln(X(t)/X(0)) = mu*time
## ln(X(t)) = ln(X(0)) + mu*time

## using values similar to our experiments
x0 <- 0.05 # the OD we inoculate to
t2 <- 1 # doubling time, hour; 1 division per hour
mu <- log(2)/t2 # growth rate, hour^-1; log(X(t2)/X(0))/(t2-0) = mu 

## model exponential growth for 10 h
time <- seq(0, 10, .1)
xt <- x0 * exp(mu*time) 

## add noise
SD <- .5 # standard deviation of normally distributed noise to be added
noise <- rnorm(length(xt), mean=0, sd=SD)

## inspect the noise
hist(noise)
## .. and add it to the data
xn <- xt + noise ## add some noise

## inspect model plus noise
plot(time, xn)
lines(time, xt)

## log version -> linear
plot(time, xn, log="y") ## WHY more noise at low values ?
lines(time, xt)

## put all in a data.frame
df <- data.frame(time=time, OD=xn, realOD=xt)

plot(df$time, df$OD,log="y")
lines(time, df$realOD)

## add log version 
## NOTE: in R the default base of log is e, ie. we are calculating ln(OD) here
df$lgOD <- log(df$OD)
plot(df$time, df$lgOD)
abline(a=log(x0), b=mu) # test against original noise-free data - WHY log(x0) ?

##  linear fit ln(X(t)) = ln(X(0) + mu * time
lmfit <- lm(lgOD ~ time, data=df)

## get the fitted parameters (called coefficients in R)
parameters <- unlist(coef(lmfit))

## plot the result: intercept and slope
abline(a=parameters[1], b=parameters[2],col=2,lwd=2)

## plot without log 
plot(df$time, df$OD)
lines(df$time, x0*exp(mu*time))
lines(df$time, exp(parameters[1]) * exp(parameters[2]*df$time),col=2, lwd=2)


## intermezzo: the exponential function and Euler's number
x <- seq(1,10,.1)
plot(x, exp(x), xlim=c(1,10),type="l")
for ( xx in c(6,8,9) ) {
  abline(a=exp(xx)*(1-xx), b=exp(xx),col=2)
  points(xx,exp(xx),pch=4,col=2,cex=2)
}
text(5,exp(9), expression(f(x)~"="~"f'"(x)~"="~e^x),bty="n",cex=2)


### NON-LINEAR MODELS
## require initialization!

## trick 17: use the results of the linear fit as initial guesses
## for a non-linear fit
start <- list(mu=parameters[2], x0=exp(parameters[1]))
nlfit <- nls(OD ~ x0*exp(mu*time), data=df, start=start) # TRY MORE COMPLEX MODELS, see a few slides ahead
nlparams <- coef(nlfit)

## SUMMARY
plot(df$time, x0*exp(mu*df$time), type="l",col=1) # orignal model!
points(df$time, df$OD) # data with noise
lines(df$time, exp(parameters[1]) * exp(parameters[2]*df$time),col=2, lwd=2)
lines(df$time, nlparams["x0"] * exp(nlparams["mu"]*df$time),col=4, lwd=2)
legend("topleft", 
       legend=c("model", "model+noise", "linear fit", "non-linear fit"),
       col=c(1,1,2,4),
       lty=c(1,NA,1,1),
       pch=c(NA,1,NA,NA))
legend("bottomright", bty="n",
       legend=c("nls fit:",
                paste("X0:", round(nlparams["x0"],3)),
                paste("mu:", round(nlparams["mu"],3)),
                paste("noise:", round(sd(residuals(nlfit)),3))))

## SUMMARY LOGGED
plot(df$time, x0*exp(mu*df$time), type="l",col=1, log="y", 
     ylab="OD", xlab="time, h") # orignal model!
points(df$time, df$OD, col=1) # data with noise
lines(df$time, exp(parameters[1]) * exp(parameters[2]*df$time),col=2, lwd=2)
lines(df$time, nlparams["x0"] * exp(nlparams["mu"]*df$time),col=4, lwd=2)
points(df$time, nlparams["x0"] * exp(nlparams["mu"]*df$time),col=4, pch=4)
legend("topleft", 
       legend=c("model", "model+noise", "linear fit", "non-linear fit"),
       col=c(1,1,2,4),
       lty=c(1,NA,1,1),
       pch=c(NA,1,NA,4))
legend("bottomright", bty="n",
       legend=c("nls fit:",
                paste("X0:", round(nlparams["x0"],3)),
                paste("mu:", round(nlparams["mu"],3)),
                paste("noise:", round(sd(residuals(nlfit)),3))))



### ESTIMATE ADDED NOISE

## get predicted values
lmx <- predict(lmfit, newdata=list(time=df$time))
nlx <- predict(nlfit, newdata=list(time=df$time))

## calculate residuals for lm and nls for original raw data
## to which we had added noise
## RESIDUALS = OBSERVED - PREDICTED
reslm <- df$OD - exp(lmx)   # ln(X(t)) was fitted, exp to compare
resnl <- df$OD - nlx        # = sd(residuals(nlfit))

## standard deviation of residuals for nls
sd(resnl, na.rm=T)
sd(residuals(nlfit))

## standard deviation of residuals for lm
sd(reslm, na.rm=T)

## ADVANCED QUESTIONS
## * Did you get any "warnings"? What do they mean? -> google the warning messages!
## * Why does the non-linear fit perform better?
## * How could you improve the performance of the linear fit?
## * Try different types of noise; can you add noise that is proportional
##   to the measurement?
## * Why is is noise estimation from lm so off, while nls gets it quite well?



## ADVANCED EXERCISES 
## * vary parameters systematically and compare lm vs. nls results 
## * increase noise
## * decrease total measurement time
## * increase doubling time
## What effects can you see?


time <- seq(0,7.5,0.1) # hours
t2 <- 1 # doubling time in hours
mu <- log(2)/t2 # specific growth rate, hour^-1 # Q: WHY log(2)/t2 ?
x0 <- 0.05 # the inoculum: cell density, cells liter^-1
xt <- x0 * exp(mu*time)
SDS <- seq(0,1.5,.1) # increaseing standard deviation of added noise!

## matrix of standard deviation of residuals of lm and nls fits
## once for X(t), once for ln(X(t))
SDM <- data.frame(realSD=SDS, lmSD=NA, nlSD=NA, lmSDlg=NA, nlSDlg=NA)


pdf("test_lm_nls.pdf") # REDIRECT PLOT TO PDF

for ( i in 2:length(SDS) ) {
  
  ## add noise
  sd <- SDS[i]
  xn <- xt + rnorm(length(xt), sd = sd)
  
  ## linear fit of the noisy data
  lfit <- lm(log(xn) ~ time)

  ## use lm results as initial guesses for nls
  lfitp <- coef(lfit)
  nlfit <- nls(xn ~ x0*exp(mu*time), start=list(x0=exp(lfitp[1]), mu=lfitp[2]),
               control=nls.control(maxiter = 500))
  
  ## get predicted values
  lx <- predict(lfit,newdata=list(time=time))
  nlx <- predict(nlfit,newdata=list(time=time))

  # r-squared?
  summary(lfit)$r.squared   # fraction of variation that can be explained by the linear model
  summary(nlfit)$r.squared  # not defined for nls!

    
  ## calculate residuals for lm and nls for 
  ## both log and non-log values
  ## RESIDUALS = OBSERVED - PREDICTED
  reslmrw <- xn - exp(lx)        # raw data ln(X(t)) 
  resnlrw <- xn - nlx            # = residuals(nlfit))
  reslmlg <- log(xn) - lx        # = residuals(lfit)
  resnllg <- log(xn) - log(nlx)  # logged data X(t)

  ## compare residuals
  ## store SD of residuals
  SDM[i,"nlSD"] <- sd(residuals(nlfit)) # should equal sd(resnlrw, na.rm=T) !
  SDM[i,"lmSD"] <- sd(reslmrw, na.rm=TRUE)  
  SDM[i,"nlSDlg"] <- sd(resnllg, na.rm=TRUE)  
  SDM[i,"lmSDlg"] <- sd(residuals(lfit)) # should equal sd(reslmlg, na.rm=T)
  
  ## compare results
  par(mfrow=c(2,2),mai=c(.75,.75,.1,.1),mgp=c(1.5,.5,0),cex=1.2)
  ## X(t)
  plot(time, xn, xlab="t: time, h",ylab=expression(X(t)~"="~X(0)*e^{mu*t}))
  lines(time, xt, col=1)
  lines(time, exp(lx), col=2, lwd=3)
  points(time, nlx, col=4,pch=4)
  legend("topleft", 
         legend=c("model", "model+noise", "linear fit", "non-linear fit"),
         col=c(1,1,2,4), lty=c(1,NA,1,NA), pch=c(NA,1,NA,4), lwd=c(1,1,3,NA))
  ## log(X(t))
  plot(time, log(xn), xlab="t: time, h",ylab=expression(ln(X(t))~"="~ln(X(0)) + mu*t))
  lines(time, log(xt), col=1)
  lines(time, lx, col=2, lwd=3)
  points(time, log(nlx), col=4,pch=4)
  plot(xn, reslmrw, col=2, ylab="residuals", xlab=expression(X(t)))
  points(xn, resnlrw,col=4)
  abline(h=0)
  plot(log(xn), reslmlg, col=2, ylab="residuals", xlab=expression(ln(X(t))))
  points(log(xn), resnllg, col=4)
  abline(h=0)
  
}

## residuals SD is the input SD for the nls model
par(mfrow=c(3,2),mai=c(.75,.75,.1,.1),mgp=c(1.5,.5,0),cex=1.2)
plot(SDM[,1], SDM[,"nlSD"], xlab="added SD", ylab="nls residual SD", ylim=range(SDS))
abline(a=0, b=1, col=2, lwd=2)
plot(SDM[,1], SDM[,"lmSD"], xlab="added SD", ylab="lm residual SD", ylim=range(SDS))
abline(a=0, b=1, col=2, lwd=2)
plot(SDM[,1], SDM[,"lmSDlg"], xlab="added SD", ylab="LOG lm residual SD")
plot(SDM[,1], SDM[,"nlSDlg"], xlab="added SD", ylab="LOG nls residual SD")

plot(SDM[,"lmSD"], SDM[,"nlSD"], ylab="nls residual SD", xlab="lm residual SD")
abline(a=0, b=1, col=2, lwd=2)
plot(SDM[,"lmSDlg"], SDM[,"nlSDlg"], ylab="nls residual SD", xlab="lm residual SD")
abline(a=0, b=1, col=2, lwd=2)
dev.off()


