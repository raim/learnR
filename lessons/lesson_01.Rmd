---
title: "Learning R - Lesson 01"
output: html_document
bibliography: ../learnR.bib
---

Further reading and background information on lesson 01. 

See [R code for lesson 01](lesson_01.R).

## Online Ressources

Tutorials and further reading on exponential growth:

* [Nature Education: How Populations Grow: The Exponential and Logistic Equations](https://www.nature.com/scitable/knowledge/library/how-populations-grow-the-exponential-and-logistic-13240157)
* [Uni Göttingen: Exponentielle Wachstumsprozesse](https://lp.uni-goettingen.de/get/text/4908) (in german)
* [Saylor academy: Observations about the exponential function](https://www.saylor.org/site/wp-content/uploads/2011/06/MA221-2.1.1.pdf)
* [Wikipedia: like $\pi$, $e$ is a transcendental number](https://en.wikipedia.org/wiki/E_(mathematical_constant))
* @Hall2014 : Growth Rates Made Easy. (see literature list below)

Tutorials and further reading on linear & non-linear regression:

* [Online tutorial on simple linear regression at r-tutor](http://www.r-tutor.com/elementary-statistics/simple-linear-regression)
* [Non-linear least squares, short tutorial at R-bloggers](https://www.r-bloggers.com/first-steps-with-non-linear-regression-in-r/)
* [Wikipedia: least squares](https://en.wikipedia.org/wiki/Least_squares)

# Mathematical Background

## Bacterial Growth

Let's assume our bacterium divides once each time unit:

$$
\begin{aligned}
X(0) &= 1\\
X(1) &= 2 = 2 \cdot X(0) &= 2^1 \cdot X(0)\\
X(2) &= 4 = 2 \cdot X(1) = 2 \cdot 2 \cdot X(0) &= 2^2 \cdot X(0)\\
X(3) &= 8 = 2 \cdot X(2) = 2 \cdot 2 \cdot 2 \cdot X(0) &= 2^3 \cdot X(0)\\
 ...& \\
X(t) &= &= 2^t \cdot X(0) 
\end{aligned}
$$

... or generally $X(t) = R^t X(0)$, where each bacterium produces $R$ new cells
per time unit. $R$ is sometimes called the "finite rate of population increase".

## Exponential Growth

You should know the equation $X(t) = e^{\mu t} X(0)$: looks similar, right? But 
using $R=e^{\mu}$ ($\mu = \ln R$) instead. So how does Euler's number $e$ get in there?

<center> "__[like $\pi$, $e$ is a transcendental number](https://en.wikipedia.org/wiki/E_(mathematical_constant))__" </center>
</br>

The short answer: it makes doing math with these equations much easier. Euler's number is special. From [wikipedia](https://en.wikipedia.org/wiki/E_(mathematical_constant)): "The function $f(x)=e^x$ is called the (natural) exponential function, and is the unique exponential function of type $a^x$ equal to its own derivative $f(x) = f′(x) = e^x$." That is, the value f(x) also describes the slope of the function, at each point.

```{r, echo=FALSE}
x <- seq(1,10,.1)
plot(x, exp(x), xlim=c(1,10),type="l")
for ( xx in c(6,8,9) ) {
  abline(a=exp(xx)*(1-xx), b=exp(xx),col=3, lwd=2)
  points(xx,exp(xx),pch=4,col=2,cex=2)
}
text(5,exp(9), expression(f(x)~"="~"f'"(x)~"="~e^x),bty="n",cex=2)
```

In contrast, it is very hard to get the derivative of general exponential functions.
For example, the time derivative of above equation is: 

$$
\begin{aligned}
X(t) &= R^t \, X(0)\\
\frac{\text{d}X(t)}{\text{d}t} &= \ln(R) \, R^t \, X(t) \;,
\end{aligned}
$$

ie. the derivative of the function contains the function itself. (Note how we
silently dropped the multiplication sign '$\cdot$' which is now implied; math peeps
have a knack for elegance, this just looks nicer).

If we replace $R$ by Euler's magic number with an exponent $e^\mu$ things gets simpler:

$$
\begin{aligned}
X(t) &= e^{\mu t} X(0)\\
\frac{\text{d}X}{\text{d}t} &= \mu \, X \;.
\end{aligned}
$$

**Wow, math magic!** As an exercise you can try to do this differentiation,
by applying the chain rule of differentiation and the equivalence 
$f(x)=f'(x)=e^x$.

### Alternative approach

Alternatively, we can approach the problem from the other side, starting
with a more general observation that the increase in population size is always proportional (increases linearly with) to the current population size.

$$
\frac{\text{d}X}{\text{d}t} \propto X
$$

Let's assume some factor $\mu$, and from there we do some
math magic:

$$
\begin{aligned}
\frac{\text{d}X}{\text{d}t} &= \mu X\\
\frac{\text{d}X}{X} &= \mu \, \text{d}t\\
\int_{X_0}^{X} \frac{1}{X} \text{d}X &= \int_{0}^{t} \mu \, \text{d}t\\
\ln{X} - \ln{X_0} &= \mu\,(t-0)\\
\ln \frac{X}{X_0} &= \mu\,t\\
\frac{X}{X_0} &= e^{\mu t}\\
X &= X_0 \, e^{\mu t}
\end{aligned}
$$

Note, how Euler's number entered the equation as the integral of $\frac{1}{X}$.


### Advanced Exercises

* R/Math: re-create above plot using `abline`, `points` and $f(x)=f'(x)=e^x$
* Math: calculate the doubling time $T_2$
* Math: calculate the time derivative of $X(t) = e^{\mu t} X(0)$

# Statistics Background

## Principles of Data Fitting

Most model fitting algorithms are based on the `least squares` approach. They attempt
to minimize the squares of the residuals, the residuals are the differences
between the obtained model and the original data. You can access the residuals
from a `fit` object (the result of calling `lm` or `nls`) by

`residuals(fit)`

## Quality of a Fit

How can we estimate the quality of the fits? It is simple for linear regression;
in short: `summary(fit)$r.squared` is 1 for a perfect fit and 0 for nonsense. But
in general and especially for more complex approaches such as non-linear regression
this is a very complex topic with no clear answer. Quoting the experts: 

* [Dirk Eddelbuettel's answer at stackoverflow](https://stats.stackexchange.com/questions/4510/how-to-compute-goodness-of-fit-for-a-linear-model-in-r):

"It all starts with

`summary(fit)`

after your fit. There are numerous commands to assess the fit, test commands, compare alternative models, ... in base R as well as in add-on packages on CRAN.

But you may want to do some reading, for example with Dalgaard's book or another introduction to statistics with R."

* [Paul Hiemstra at stackoverflow](https://stackoverflow.com/a/14530791) on why r-squared is not a good measure for non-linear fits: "Note that the r squared is not defined for non-linear models, or at least very tricky"

* [Low R-squared values are not always good, high not always bad](http://blog.minitab.com/blog/adventures-in-statistics-2/regression-analysis-how-do-i-interpret-r-squared-and-assess-the-goodness-of-fit): "You should evaluate R-squared values in conjunction with residual plots, other model statistics, and subject area knowledge in order to round out the picture (pardon the pun)."

In summary, you have to inspect the residuals.


### Advanced Exercises

* Can you re-create the amount of noise you added to the original modelled
data from the `lm` and `nls` fits?
* The `R-squared` measure is not defined for non-linear regression, can you 
still calculate an `R-squared` for the `nls` fit that is directly comparable 
to the `R-squared` obtained from `lm` (`summary(lmfit)$r.squared`)?

# Literature
