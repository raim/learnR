# Learning R (to fit growth rates and analyze gene expression)

This is an evolving crash course for R, specifically for learning
how to analyze microbial growth curves and reporter gene expression.

Open the file [learnR.Rmd](learnR.Rmd) in `rstudio` and click the `knitr` button
to learn more.
